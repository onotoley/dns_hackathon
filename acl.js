var ACL = {
    allow   : { 'bootstrapcdn.com' : null,
                'googleapis.com' : null,
                'cloudflare.com' : null },
    deny    : {},
    pending : {},
    domainName : function(uri) {
        var split = uri.split('.');
        var len = split.length;
        if (len < 2)
            return uri;
        return split[len-2] + '.' + split[len-1];
    }
}
module.exports = ACL;
