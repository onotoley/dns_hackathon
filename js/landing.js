angular.module('dns-child', ['ui.bootstrap'])
.controller('appMain', function($scope, $http, $timeout) {
    $scope.status = 'pending';
    $scope.update = function() {
        $http.get("/status").then(function(response) {
            $scope.status = response.data.status;
            if ($scope.status == 'denied' || $scope.status == 'allowed')
                window.location.reload(true);
        });
    };
    $scope.start_update = function() {
        function do_update() {
            $scope.update();
            setTimeout(do_update, 1000);
        }
        do_update();
    };
    $scope.start_update();
});
