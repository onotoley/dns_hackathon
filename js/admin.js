angular.module('dns-parent', ['ui.bootstrap'])
.controller('appMain', function($scope, $http, $timeout) {
    $scope.data = [];
    $scope.update = function() {
        $http.get("/pending").then(function(response) {
            var updated = [];
            Object.keys(response.data).forEach(function(key,index) {
                updated.push(key);
            });
            $scope.data = updated;
        });
    };
    $scope.start_update = function() {
        function do_update() {
            $scope.update();
            setTimeout(do_update, 1000);
        }
        do_update();
    };
    $scope.allow = function(name) {
        $http.get("/allow?" + name);
    };
    $scope.deny = function(name) {
        $http.get("/deny?" + name);
    };
    $scope.start_update();
});
