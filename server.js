var path = require('path');
var WebServer = require('./web_server.js');
var DnsProxy = require('./dns_proxy.js');

var root = path.dirname(process.argv[1]);

var web_server = new WebServer(root, 80);
var dns_proxy = new DnsProxy(web_server.ip);

