var acl = require('./acl.js');
var dgram = require('dgram')
var packet = require('native-dns-packet')

function DnsProxy(ip) {
    var _this = this;
    this.ip = ip;
    this.nameserver = '8.8.8.8';
    this.server = dgram.createSocket('udp4');
    this.server.on('message', function (message, rinfo) {
        var request = packet.parse(message);
        // console.log('dns request = ' + JSON.stringify(request));
        var sock = dgram.createSocket('udp4');
        sock.on('message', function(response) {
            var parsed = packet.parse(response);
            if (parsed.answer[0] && (parsed.answer[0].type == 1 ||
                                     parsed.answer[0].type == 5)) { // A || CNAME
                var host = parsed.answer[0].name;
                var dn = acl.domainName(host);
                var reserialize = false;
                if (acl.deny[dn] === null) {
                    for (var a in parsed.answer) {
                        parsed.answer[a].address = _this.ip;
                        reserialize = true;
                    }
                } else if (acl.allow[dn] === undefined) {
                    acl.pending[dn] = null;
                    for (var a in parsed.answer) {
                        parsed.answer[a].address = _this.ip;
                        parsed.answer[a].ttl = 0;
                        reserialize = true;
                    }
                }
                if (reserialize) {
                    var buffer = new Buffer(4096);
                    var len = packet.write(buffer, parsed);
                    response = buffer.slice(0, len);
                }
            }
            // console.log('dns response = ' + JSON.stringify(parsed));
            _this.server.send(response, rinfo.port, rinfo.address);
            sock.close();
        });
        sock.send(message, 53, _this.nameserver);
    });
    this.server.bind(53, this.ip);
    console.log('DNS server bound to: udp://%s:%s', _this.ip, 53);
}
module.exports = DnsProxy;
