var os = require('os');
var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');
var acl = require('./acl.js');


function WebServer(root, port) {
    var _this = this;
    
    this.root = root;
    this.port = port;
    
    function getIpAddress() {
        var ifaces = os.networkInterfaces();
        var ip = undefined;
        Object.keys(ifaces).every(function(ifname) {
            ifaces[ifname].every(function(iface) {
                if (!iface.internal && 'IPv4' === iface.family) {
                    ip = iface.address;
                    return false;
                }
                return true;
            });
            return ip === undefined;
        });
        return ip;
    }

    this.ip = getIpAddress();

    function handleRequest(request, response) {
        var parsed = url.parse(request.url, true);
        //console.log('http request: ', JSON.stringify(request.headers));
        function mime(ext) {
            switch (ext) {
                case '.htm':
                case '.html':
                    return 'text/html';
                case '.js':
                    return 'text/javascript';
                case '.css':
                    return 'text/css';
                case '.jpg':
                case '.jpeg':
                    return 'image/jpeg';
                case '.png':
                    return 'image/png';
            }
            return 'text/plain';
        }
        function error(status, message) {
            response.writeHead(status, {'Content-Type': 'text/html'});
            response.end('<html><head></head><body>' + status + ': ' + message + '</body></html>');
        }
        function reply(mime, data) {
            response.writeHead(200, {'Content-Type': mime, 'Cache-Control': 'private, no-cache, must-revalidate, no-store'});
            response.end(data);
        }
        function file(file) {
            file = _this.root + '/' + file;
            fs.exists(file, function(exists) {
                if (exists) {
                    fs.readFile(file, function(err, content) {
                        if (err) {
                            error(500, 'Interval server error: ' + err.message);
                        } else {
                            reply(mime(path.extname(file)), content);
                        }
                    });
                } else {
                    error(404, 'Not found');
                }
            });
        }
        function ok() {
            reply('application/json', '{"success":true}');
        }
        var dn = acl.domainName(request.headers.host);
        if (parsed.pathname == '/status') {
            if (acl.allow[dn] === null)
                reply('application/json', '{"status":"allowed"}');
            else if (acl.deny[dn] === null)
                reply('application/json', '{"status":"denied"}');
            else
                reply('application/json', '{"status":"pending"}');
        } else if (acl.deny[dn] === null) {
            file('html/access_denied.html');
        } else {
            var resource_regex = /.*\.(html|js|css|png|jpg)$/;
            if (resource_regex.test(parsed.pathname)) {
                file(request.url);
            } else {
                switch(parsed.pathname) {
                    case '/admin':
                        file('html/admin_page.html');
                        break;
                    case '/list':
                        reply('application/json', JSON.stringify(acl));
                        break;
                    case '/pending':
                        reply('application/json', JSON.stringify(acl.pending));
                        break;
                    case '/allow':
                        if (parsed.query) {
                            var dn = acl.domainName(parsed.search.substring(1));
                            acl.allow[dn] = null;
                            delete acl.pending[dn];
                        }
                        ok();
                        break;
                    case '/deny':
                        if (parsed.search) {
                            var dn = acl.domainName(parsed.search.substring(1));
                            acl.deny[dn] = null;
                            delete acl.pending[dn];
                        }
                        ok();
                        break;
                    default:
                        file('html/landing_page.html');
                }
            }
        }
    }
    
    this.server = http.createServer(handleRequest);
    this.server.listen(this.port, function(){
        console.log('Web server listening on: http://%s:%s', _this.ip, _this.port);
    });
}
module.exports = WebServer;
